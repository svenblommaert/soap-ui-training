# SOAP UI Training

[Rest Countries](https://restcountries.eu/) - https://restcountries.eu/
- REST Service
- Exersises (tips --> https://support.smartbear.com/alertsite/docs/monitors/api/endpoint/jsonpath.html)
- Get Countries in the Europe Region, how much?  
- Countries with population less then 28875
- name of Country where population is 5491817
- name of Country wher lat(0) is 64.0 and lon(1) is 26.0

[DNE Online Calculator](http://www.dneonline.com/calculator.asmx) - http://www.dneonline.com/calculator.asmx
- SOAP service
- 

[Petstore Swagger](https://petstore.swagger.io/) - https://petstore.swagger.io/
- REST service
- POST/GET/PUT/DELETE - Create Read Update Delete 
- Exercises
- Create a pet - Verify that the pet is created
- Create a pet - Delete that pet - Verify Deleted pet
- Create a pet - Update pet name - Verify pet has a correct name

[Donald Trump](https://docs.tronalddump.io/) - https://docs.tronalddump.io/
- REST Service
- get 1 random qoute
- Add a step and get the same quote again


